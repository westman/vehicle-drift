addpath(genpath('../'))

pars.t0= 0;
pars.T= 10;
pars.dt= 1.0000e-03;
pars.x0= [
   -0.3567;
    0.6000;
    8.0000
];
pars.u0= [0;0];
pars.vs0= [0;0;0];
pars.g= 9.8000;
pars.m= 1724;
pars.Iz= 1300;
pars.a= 1.3500;
pars.b= 1.1500;
pars.L= 2.5000;
pars.CaF= 120000;
pars.CaR= 175000;
pars.mu= 0.5500;
pars.FzF= 7.7718e+03;
pars.FzR= 9.1234e+03;
pars.delta_eq= -0.2094;
pars.Ux_eq= 8;
pars.beta_eq= -0.3567;
pars.r_eq= 0.6000;
pars.FxR_eq= 2293;
pars.FyF_eq= 3807;
pars.FyR_eq= 4469;
pars.x_eq= [   -0.3567;
    0.6000;
    8.0000];
pars.FxR_max= 3.5125e+03;
pars.K_beta= 2;
pars.K_r= 4;
pars.K_Ux= 0.8460;


x = [-0.3602;
    0.4667;
   10.1960];

u_plus = [pars.delta_eq; pars.FxR_eq];
T = 100;
DX = NaN(T,3);
X = NaN(T,3);
for i = 1:T
    
    DX(i,:) = Dynamics(x,u_plus,pars);
    x = x + pars.dt*DX(i,:)';
    X(i,:) = x;
    
    figure(1); clf; subplot(3,1,1)
    plot(X(1:i,1),'r'); title('Beta')
    subplot(3,1,2);
    plot(X(1:i,2),'b'); title('r')
    subplot(3,1,3)
    plot(X(1:i,3),'g'); title('Ux')
%     pause(1)
    hold off
end



