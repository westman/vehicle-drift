
Ux_eq = 8;                  % m / s

pars = GetParameters();

betas = -0.8:0.05:0;
rs = 0:0.1:1.6;

% [delta, FxR]
us = NaN(size(meshgrid(rs,betas)));
vs = NaN(size(meshgrid(rs,betas)));
for i = 1:length(rs)
    for j = 1:length(betas)
        beta = betas(i);
        r = rs(j);
        dx = Dynamics([beta, r, Ux_eq], [0,0], pars);
        us(i,j) = dx(1);
        vs(i,j) = dx(2);
    end
end

streamslice(betas,rs,us,vs)


% Beta on x axis, r on y axis
